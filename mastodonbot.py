# bot die de meest aangeklikte wikipediapagina van gisteren opzoekt, 
# en daarover toot op Mastodon, inclusief plaatje!
from mastodon import Mastodon
import json
from datetime import date, timedelta
import requests
from io import BytesIO
import os
from PIL import Image
import re

# get secret token
keyFile = open('keys', 'r')
lines = keyFile.readlines()
access_token = lines[0].rstrip()

# authorize mastodon
mastodon = Mastodon(
    access_token = access_token,
    api_base_url = 'https://botsin.space'
)

# header for requests
user_agent = {'User-agent': 'Mozilla/5.0'}

# change this to your language of choice!!
# for inspiration, look here: https://en.wikipedia.org/wiki/List_of_Wikipedias#Basic_list
language = "sv"

# if you change the language, you also need to change the blacklist
blacklist = ["Huvudsida", "Special:Sök", "Special:SenasteÄndringar"]


#make date variables
yesterday = date.today() - timedelta(days=1)
year = yesterday.strftime("%Y")
month = yesterday.strftime("%m")
day = yesterday.strftime("%d")
printday = yesterday.strftime("%-e")
months_in_swedish = ["januari", "februari", "mars", "april", "maj", "juni", "juli", "augusti", "september", "oktober", "november", "december"]
printmonth = months_in_swedish[int(month)-1]

#function to turn the amount of views into a printable number with dots/commas
def number_with_decimal_seperators(nr):
    nr = str(nr)
    #Replace decimal_seperator with "," for an English layout
    decimal_seperator = "."
    print_amount = ""
    counter = 0
    for i in range(len(nr)-1,-1,-1):
        counter += 1
        print_amount = nr[i] + print_amount
        if counter % 3 == 0:
            print_amount = decimal_seperator + print_amount
    if print_amount[0] == decimal_seperator:
        print_amount = print_amount[1:]
    return print_amount

#function to vary the toot text according to the amount of views
def how_many_times(amount):
    if amount > 100000:
        return "med astronomiska"
    elif amount > 50000:
        return "med gigantiska"
    elif amount > 30000:
        return "med imponerande"
    elif amount > 21000:
        return "med rejäla"
    elif amount > 12000:
        return "med hela"
    elif amount > 9000:
        return "med fina"
    else:
        return "med"

def toot_toot():
    #get yesterday's top viewed pages
    req_url = "https://wikimedia.org/api/rest_v1/metrics/pageviews/top/{}.wikipedia.org/all-access/{}/{}/{}".format(language, year, month, day)
    r = requests.get(req_url, headers = user_agent)
    #print(r.status_code)
    response_data = r.json()
    #determine top viewed page, excluding blacklisted items
    for i in range(5):
        most_viewed_page = response_data["items"][0]["articles"][i]['article']
        if most_viewed_page in blacklist:
            continue
        else:
            most_viewed = response_data["items"][0]["articles"][i]['article']
            amount_of_views = response_data["items"][0]["articles"][i]['views']
            break
    most_viewed_url = "https://" + language + ".wikipedia.org/wiki/" + most_viewed
    print("🥇 " + most_viewed_url)
    # get picture url and plain text title
    page_info_url = "https://{}.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles={}".format(language, most_viewed)
    r = requests.get(page_info_url, headers = user_agent)
    response = r.json()
    try:
        top_title = response["query"]["normalized"][0]["to"]
        print(top_title)
    except:
        print("no top title yet...")
    # try to find picture
    try:
        number_dict = response["query"]["pages"]
        number = list(number_dict.keys())[0]
        top_title = response["query"]["pages"][number]["title"]
        print(top_title)
        picture_url = response["query"]["pages"][number]["original"]["source"]
        print(picture_url)
    except:
        picture_url = None
        print("🚫 no picture here")
    # make relevant hashtag
    hashtag_blacklist = [" ", "&", "-", ".", "Kategori:", "Lista över"]
    hashtag = top_title
    for item in hashtag_blacklist:  
        if item in hashtag:
            hashtag = hashtag.replace(item, "")
    if "(" or ":" in hashtag: 
        hashtag = re.split('\(|:', hashtag)[0]
    # make print message!
    print_message = """\"{}\" var den mest besökta wikipediaartilken  {} {} {}, {} {} visningar!
    {}
    #wikipedia
    #{}""".format(top_title, printday, printmonth, year, how_many_times(amount_of_views), number_with_decimal_seperators(amount_of_views), most_viewed_url, hashtag)
    print("📝  print message is: " + print_message)
    # image stuff
    if picture_url is not None:
        filename = 'temp.'
        response = requests.get(picture_url, headers = user_agent)
        try:
            img = Image.open(BytesIO(response.content))
            picture_extension = img.format
            print("picture type is: " + picture_extension)
            filename += picture_extension
            img.save(filename)
            picture_kb= os.stat(filename).st_size
            print("🖼️ Picture is {} bits".format(str(picture_kb)))
            if picture_kb >= 800000:
                image = Image.open(filename)
                image.thumbnail((500,500))
                image.save(filename)
                picture_kb= os.stat(filename).st_size
                print("🖼️ Resized picture is {} bits".format(str(picture_kb)))
            media = mastodon.media_post(filename, "image/jpeg")
            #"Images (PNG, JPG, GIF) up to 8MB. Images will be downscaled to 1.6 megapixels (enough for a 1280x1280 image). Up to 4 images can be attached."
            # https://docs.joinmastodon.org/user/posting/#attachments)
            mastodon.status_post(status=print_message, media_ids=[media])
            print("🎺 tooted! (with pic)")
            os.remove(filename)
        except Exception as e: 
            print(e)
            mastodon.status_post(status=print_message)
            (print("🎺 tooted! (pic unfit for toot)"))
    else:
        mastodon.status_post(status=print_message)
        (print("🎺 tooted! (no pic)"))

toot_toot()
